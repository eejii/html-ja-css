let type = document.getElementById("type-input");
let todo = document.getElementById("todo");
let receive = document.getElementById("receive-input");
let form = document.getElementById("form");

document.addEventListener("submit", submitHandler);

function addListItem(text) {
  // TODO: implement this function
  let li = document.createElement('li');
  li.innerText = text;
  todo.append(li);
};

function submitHandler(e) {
  // TODO: implement this function
  e.preventDefault();
  
   if (type.value != null && type.value != "") {
      addListItem(type.value);
      type.value = "";
   }
}

todo.onclick = function listClickHandler(e) {
  // TODO: implement this function
   if(e.target.classList.contains("done") != true) {
      e.target.classList.add("done");
   }
   else {
         let parentEl = e.target.parentNode;
         parentEl.removeChild(e.target);
      }
}
